let collection = [];

let indexOne = 0;
let indexTwo = 0; 

function print() {
  const result = [];
  for (let i = indexOne; i < indexTwo; i++) {
    result[result.length] = collection[i];
  }
  return result;
}

function enqueue(item) {
  collection[indexTwo] = item;
  indexTwo++;
  return print();
}

function dequeue() {
  if (indexOne === indexTwo) {
    return print();
  }

  const frontItem = collection[indexOne];
  indexOne++;

  const newCollection = {};
  let newIndex = 0;
  for (let i = indexOne; i < indexTwo; i++) {
    newCollection[newIndex] = collection[i];
    newIndex++;
  }
  collection = newCollection;

  indexTwo = newIndex;
  indexOne = 0;

  return print();
}

function size() {
  return indexTwo - indexOne;
}

function front() {
    return collection[0];
}

function isEmpty() {
    return collection.length === 0;
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty
};